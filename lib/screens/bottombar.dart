import 'package:fluentui_icons/fluentui_icons.dart';
import 'package:flutter/material.dart';
import 'package:ticket_booking/screens/home_screen.dart';

class Botombutton extends StatefulWidget {
  const Botombutton({Key? key}) : super(key: key);

  @override
  State<Botombutton> createState() => _BotombuttonState();
}

class _BotombuttonState extends State<Botombutton> {
  int _selectIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectIndex = index;
    });
  }

  static final List<Widget> _WidgetOPtions = <Widget>[
    Home(),
    const Text('Ticket'),
    const Text('Search'),
    const Text('Profil')
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _WidgetOPtions[_selectIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectIndex,
        onTap: _onItemTapped,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.blueGrey,
        unselectedItemColor: Color(0xFF526480),
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              FluentSystemIcons.ic_fluent_home_regular,
            ),
            activeIcon: Icon(
              FluentSystemIcons.ic_fluent_home_filled,
            ),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: Icon(
              FluentSystemIcons.ic_fluent_ticket_regular,
            ),
            activeIcon: Icon(
              FluentSystemIcons.ic_fluent_ticket_filled,
            ),
            label: "Airplane",
          ),
          BottomNavigationBarItem(
            icon: Icon(
              FluentSystemIcons.ic_fluent_search_regular,
            ),
            activeIcon: Icon(
              FluentSystemIcons.ic_fluent_search_filled,
            ),
            label: "Search",
          ),
          BottomNavigationBarItem(
            icon: Icon(
              FluentSystemIcons.ic_fluent_person_regular,
            ),
            activeIcon: Icon(
              FluentSystemIcons.ic_fluent_person_filled,
            ),
            label: "Profil",
          ),
        ],
      ),
    );
  }
}
